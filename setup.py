from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="mkdocs-gitlab-static-site-editor",
    version="0.2.0",
    description="MkDocs plugin for the GitLab Web Editor.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    keywords="mkdocs plugin gitlab static site editor",
    url="https://gitlab.com/nejch1/mkdocs-gitlab-static-site-editor",
    author="Nejc Habjan",
    author_email="hab.nejc@gmail.com",
    license="MIT",
    install_requires=["mkdocs>=1.0.0"],
    python_requires=">=3.6",
    classifiers=[
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "License :: OSI Approved :: MIT License",
    ],
    packages=find_packages(),
    entry_points={
        "mkdocs.plugins": [
            "gitlab-static-site-editor = mkdocs_gitlab_editor.plugin:StaticSiteEditorPlugin"
        ]
    },
)
