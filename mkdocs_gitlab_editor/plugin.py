from os.path import commonpath, relpath
from urllib.parse import quote_plus

from mkdocs.config import config_options
from mkdocs.plugins import BasePlugin


DEFAULT_BRANCH = "master"


def _get_relative_docs_dir(config):
    base_dir = commonpath([config["config_file_path"], config["docs_dir"]])
    rel_docs_dir = relpath(config["docs_dir"], base_dir).replace("\\", "/")

    return quote_plus(rel_docs_dir)


class StaticSiteEditorPlugin(BasePlugin):

    config_scheme = (("edit_branch", config_options.Type(str, default=DEFAULT_BRANCH)),)

    def on_config(self, config):
        if not config["repo_url"]:
            # Do not show edit button without user-defined repo_url
            # todo: get repo_url and remote branch from git?
            return config

        repo_url = config["repo_url"].rstrip("/")

        edit_branch = self.config["edit_branch"]
        docs_dir = _get_relative_docs_dir(config)
        edit_base_path = quote_plus(f"{edit_branch}/{docs_dir}/")

        config["editor_url"] = f"{repo_url}/-/edit/{edit_base_path}"
        return config

    def on_pre_page(self, page, config, files):
        try:
            editor_url = config["editor_url"]
        except KeyError:
            return page

        src_path = quote_plus(page.file.src_path)
        page.edit_url = f"{editor_url}{src_path}"

        if page.canonical_url is not None:
            return_url = quote_plus(page.canonical_url)
            page.edit_url = f"{page.edit_url}?return_url={return_url}"

        return page
